import java.util.Stack;

public class Hanoi {
    int numDiscs;
    Stack<Integer> towerA = new Stack<>();
    Stack<Integer> towerB = new Stack<>();
    Stack<Integer> towerC = new Stack<>();

// Konstruktor setzt die Anzahl Scheiben und initialisiert das Programm
public Hanoi(int numDiscs) {
    this.numDiscs = numDiscs;
    init();
}

// TODO: leere die Türme und fülle dann Turm A mit der korrekten Anzahl an Scheiben
public void init() {

}

// TODO: schiebe eine einzelne Scheibe von einem Turm auf einen anderen
public void moveDisc(Stack<Integer> from, Stack<Integer> to) {

}

// TODO: schiebe mehrere Scheiben von ... nach ...
//       n: Anzahl der zu bewegenden Scheiben
//    from: Der Startturm, wo die zu bewegenden Scheiben liegen
//      to: Der Zielturm, wo die Scheiben landen sollen
//  helper: Der Hilfsturm zum vorübergehenden Ablegen nicht gebrauchter Scheiben
public void moveTower(int n, Stack<Integer> from, Stack<Integer> to, Stack<Integer> helper) {
    if(true) {
        
    }
    else {
        
    }
}

// gib den Zustand aller drei Türme aus
public void printState() {
    System.out.println("Turm A:");
    System.out.println(towerA.toString());
    System.out.println("Turm B:");
    System.out.println(towerB.toString());
    System.out.println("Turm C:");
    System.out.println(towerC.toString());

    }

public void test() {
    init();
    System.out.println("Anfangszustand:");
    printState();
    moveTower(numDiscs, towerA, towerB, towerC);
    System.out.println("Endzustand:");
    printState();
}
}